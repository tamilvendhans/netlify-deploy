const express = require('express');
const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const router = express.Router();
router.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// API calls
router.get('/', (req, res) => {
  res.status(200).send({ welcome: "This is the api for dev portal website"});
});

router.get('/addItem', function(req, res, next) {
   const admin = require("./firebase-connect").admin;
    admin.database().ref("items").push().set({
      "name":  req.query.name || "",
      "dob": req.query.dob || "",
    }, (err) =>{
      if(err) {
        res.status(200).send({
          statusCode: 400,
          message: `Unable to insert the data`,
          err
        });
      }
      res.status(200).send({
          statusCode: 200,
          message: 'Ok'
        });
    })
    // res.status(200).send({ message: 'Ok' });
});

router.get('/getItems', function(req, res) {
  const admin = require("./firebase-connect").admin;
  admin.database().ref("items").once("value", function(items) {
    res.status(200).send({items})
  }, function (errorObject) {
    res.status(400).send({"Error":"The read failed: " + errorObject.code});
  })
   // res.status(200).send({ message: 'Ok' });
});

router.get('/updateItem/:id', function(req, res) {
  const admin = require("./firebase-connect").admin;
  const itemID = req.params.id || "";
  const body = {};
  if(req.query.name)
    body.name = req.query.name
  if(req.query.dob)
    body.dob = req.query.dob
  admin.database().ref("items/" + itemID).update(body, function(err) {
    if(err)
      res.status(400).send({"Error":"The read failed: " + err.message});
    res.status(200).send({ "message": "Item updated: " + itemID, body });
  });
});

router.get('/deleteItem/:id', function(req, res) {
  const admin = require("./firebase-connect").admin;
  const itemID = req.params.id || "";
  admin.database().ref("items/" + itemID).remove(function(err) {
    if(err)
      res.status(400).send({"Error":"The read failed: " + err.message});
    res.status(200).send({ "message": "Item removed: " + itemID, });
  });
});

app.use('/.netlify/functions/index', router); // path must route to lambda

module.exports = app;
module.exports.handler = serverless(app);
