var admin = require("firebase-admin");

if (admin.apps.length === 0) {
  var serviceAccount = require("../fir-express-demo-d45d7-firebase-adminsdk-tuu44-db46c0b3d5.json");
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://fir-express-demo-d45d7-default-rtdb.firebaseio.com"
  });
}

module.exports.admin = admin
module.exports.handler = async function(event, context) {
  const promise = new Promise(function(resolve, reject) {
    admin.database().ref("items").push().set({
      "name": event.queryStringParameters.name || "",
      "dob": event.queryStringParameters.dob || "",
    }, (err) =>{
      if(err) {
        reject({
          statusCode: 400,
          body: JSON.stringify({ message: `Something went wrong`, err })
        });
      }
      resolve({
          statusCode: 200,
          body: JSON.stringify({ message: `Ok`, context, event })
        });
    })
  });
  return promise;
}
