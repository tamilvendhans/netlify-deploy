// window.baseUrl = "http://localhost:9000/.netlify/functions/";
window.baseUrl = "/.netlify/functions/";
window.dataResponse = {};
bootstrap.Toast.prototype.showMessage = function(message) { 
    this._element.getElementsByClassName("toast-body")[0].innerText = message;
    this.show(); 
}
bsToast = new bootstrap.Toast(".toast");

retrieveItems();

function makeAPICall(url, data = null, options = {}, queryParams = "") {
    let config = {};
    if(data) {
        config.body = data;
    }

    if(options) {
        config.method = options.method || "GET"
        config.headers = options.headers || { "Content-Type": "application/json" }
    }
    let apiUrl = baseUrl + url + queryParams
    let promise = fetch(apiUrl, config);
    return promise;
}

function retrieveItems() {
    makeAPICall("index/getItems").then(function(res) {
        return res.json();
    }).then(function(response) {
        if(response && response.items) {
            renderDataToDOM(response.items);
        } else {
            renderDataToDOM({});
        }
    }).catch(function(err) {
        console.log(err);
    })
}

function renderDataToDOM(items) {
    window.dataResponse = items;
    let DOM = document.getElementById("data-container")
    let itemKeys = Object.keys(items);
    if(itemKeys.length) {
        let html = `<table class="table table-bordered"><tr><th>S.No</th> <th>Name</th><th>DOB</th><th>Actions</th></tr>`;
        for(let i = 0; i < itemKeys.length; i++) {
            html += `<tr id="${itemKeys[i]}"><td>${i+1}</td><td>${items[itemKeys[i]].name}</td><td>${items[itemKeys[i]].dob}</td><td><span class="btn btn-success btn-sm fa fa-pencil me-2" onclick="editItem(event)"></span><span class="btn btn-danger btn-sm fa fa-trash" onclick="removeItem(event)"></span></td></tr>`
        }
        html += `</table>`
        DOM.innerHTML = html;
    } else {
        DOM.innerHTML = `<p class="text-danger text-center">No items to list here.</p>`
    }
}

function editItem(event) {
    let itemId = event.target.parentElement.parentElement.id;
    let form = document.getElementsByTagName("form")[0];
    let selectedItem = window.dataResponse[itemId] || {};
    form.name.value = selectedItem.name || ""
    form.dob.value = selectedItem.dob || ""
    form.updateId.value = itemId
    form.submit.innerText = "Update";
}

function removeItem(event) {
    let itemId = event.target.parentElement.parentElement.id;
    makeAPICall("index/deleteItem/" + itemId).then(function(res) {
        return res.json();
    }).then(function(response) {
        if(response && response.message) {
            retrieveItems()
            bsToast.showMessage("Item Deleted.");
        } else {
            bsToast.showMessage("Unable to delete");
        }
    }).catch(function(err) {
        bsToast.showMessage("Unable to delete" + err.message);
        console.log(err);
    })
}


function handleSubmit(event) {
    event.preventDefault();
    event.target.submit.disabled = true;
    let queryParam = `?name=${event.target.name.value}&dob=${event.target.dob.value}`;
    let itemId = event.target.updateId.value;
    if(itemId) {
        makeAPICall("index/updateItem/" + itemId, null, null, queryParam).then(function(res) {
            return res.json();
        }).then(function(response) {
            if(response && response.message) {
                retrieveItems()
                bsToast.showMessage("Item Updated.");
            } else {
                bsToast.showMessage("Unable to delete");
            }
            event.target.submit.disabled = false;
            event.target.submit.innerText = "Save";
            event.target.reset();
        }).catch(function(err) {
            bsToast.showMessage("Unable to delete" + err.message);
            console.log(err);
            event.target.submit.disabled = false;
        })
    } else {
        makeAPICall("index/addItem", null, null, queryParam).then(function(res) {
                return res.json();
            }).then(function(response) {
            if(response && response.message == "Ok") {
                retrieveItems()
                bsToast.showMessage("Item added.");
            } else {
                bsToast.showMessage("Unable to delete");
            }
            event.target.submit.disabled = false;
            event.target.reset();
        }).catch(function(err) {
            bsToast.showMessage(err.message);
            console.log(err);
            event.target.submit.disabled = false;
        })
    }
}
