const express = require("express");
const path = require("path");
const localServer = express();

localServer.use(express.static(path.join(__dirname)));

localServer.get("/", function(req, res) {
    res.sendFile(path.join(__dirname+'/index.html'));
})

localServer.listen(3001, () => {
    console.log("Local server running on 3001");
})